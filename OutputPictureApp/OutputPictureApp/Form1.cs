﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace OutputPictureApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load_1(object sender, EventArgs e)
        {


        }

        private void Form1_Paint_1(object sender, PaintEventArgs e)
        {
            Bitmap bmp = new Bitmap(@"pictures/trapetsia.bmp");
            Color col = new Color();
            col = Color.FromArgb(255,181,230,29); // 1 способ: получаем цвет по argb
            Color clr = bmp.GetPixel(1, 1);  // 2 способ: получаем цвет пикселя с координатами (1,1)
            System.Drawing.Color col2 = System.Drawing.ColorTranslator.FromHtml("#ffb5e61d"); // 3 способ: получаем цвет по hex
            bmp.MakeTransparent(col2);
            e.Graphics.DrawImage(bmp, 0, 125, bmp.Width, bmp.Height);
        }


    }
}
